import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent {
  icon = 'http://s1.iconbird.com/ico/2013/9/452/w448h5121380477116trash.png';
  @Input() task = '';
  @Output() delete = new EventEmitter();
  @Output() changeTask  = new EventEmitter();

  onDeleteClick() {
    this.delete.emit();
  }

  onChangeTask(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.changeTask.emit(target.value);
  }
}
