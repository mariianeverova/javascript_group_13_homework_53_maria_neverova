import { Component } from '@angular/core';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent {
  task = '';
  tasks = [
    {task: 'Make homework #53'},
    {task: 'Change wheels on my car'},
    {task: 'Watch "Eternals"'}
  ];

  onChangeInput (event: Event) {
    const target = <HTMLInputElement>event.target;
    this.task = target.value;
  }

  onDeleteTask(i: number) {
    this.tasks.splice(i, 1);
  }

  onChangeTask(index: number, newTask: string) {
    this.tasks[index].task = newTask;
  }

  onAddTask(event: Event) {
    event.preventDefault();
    this.tasks.push({
      task: this.task
    });
    this.clear();
  }

  clear() {
    this.task = '';
  }
}
